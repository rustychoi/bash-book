# Installing Bash and Terminal

This chapter walks you through the most minimal installation and setup guide for `Bash` and `Terminal`.

This walkthrough focuses on installing the most basic tools and configuring them just enough, in order for you to be able to
follow future chapters.

This walkthrough will __NOT__ delve into the meaning of every step that we take. Rather, you should take things for granted
and follow along, and in the future we can explore the meanings of different configuration steps more.
