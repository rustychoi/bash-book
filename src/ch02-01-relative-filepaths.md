# Relative Filepaths

Filepaths are used to locate files and directories; they are like addresses to the files on your computer.

One type of filepaths you can have is called _relative_ filepaths. We'll talk about why it's called _relative_ later; let's
see some examples first.

Instead of saying `there is a file called f1.txt in a directory called d1`, you can specify the location of this file with the
filepath `d1/f1.txt`. This is much more concise.

The `/` is called a directory separator, and is used to show that what follows the `/` belongs inside what comes before it.
In this case, the `/` inside `d1/f1.txt` is used to indicate that `f1.txt` belongs inside the directory `d1`. In such cases,
we say that `d1` is the parent directory of `f1.txt`.

We can extend this idea further: if some directory `d1` has `d2` in it, and `d2` has `d3` in it, the filepath for `d3` would
be `d1/d2/d3`.

## Demo using GUI

Let's visualize this with a GUI (Graphical User Interface). I have a Mac, so I will be using `Finder` to do this.

Here you can see that we have 2 folders here, `d1` and `d2`.

![in rustychoi](./img/ch02/screenshot-rustychoi.png)

If we go into `d1` you can see below that `d1` contains `f1.txt`.
From where we are right now, the path to the file `f1.txt` is simply `f1.txt`.

![in d1](./img/ch02/screenshot-rustychoi_d1.png)

If we go __UP__ a directory such that we see the following once again, the path to `f1.txt` is no longer just `f1.txt`, but
rather, `d1/f1.txt`.

![in rustychoi](./img/ch02/screenshot-rustychoi.png)

You may have noticed that the filepath to files depends on where we are in the filesystem. When `f1.txt` is in the same
directory as us, we can specify this file with the filepath `f1.txt`, whereas if we are in the directory that contains `d1`,
as in the above screenshot, we have to use the filepath `d1/f1.txt` in order to tell the computer that `f1.txt` is not in the
current directory, but in `d1`.

### Relativity: `./`

Filepaths that we have seen thur far, like `d1` and `d1/f1.txt` are called _relative paths_ because the precise location of the 
files described by those filepaths depend on, or are `relative` to, where you are in the filesystem.

Computers use filepaths to search for a file by reading them from left to right. For example in the case of `d1/f1.txt`, the 
computer first looks for `d1`, then looks for `f1.txt` inside `d1`.

In the example that we just mentioned above, we said that "the computer first looks for `d1`" but never mentioned where exactly
it looks for `d1`. This brings up a subtle but important idea: when computers use relative paths to look for files, it starts
that search from the directory that you are in.

There is a way to write relative paths that mean the same thing as what we have been writing, but makes this point more
explicit: put a `./` at the front of the relative path. For example, `d1/f1.txt` is equivalent to saying `./d1/f1.txt`. `.`
means `current directory`, which is where you are. This way, we can more easily see that the computer takes the following steps
when using the relative path `./d1/f1.txt` to locate it:
1. find `.`, which is the directory you are in
1. find `d1` inside `.`
1. find `f1.txt` in `d1`

### Relativity: `../`

Let's consider another example; let's go into `d2`, which is empty, as shown below.

![in d1](./img/ch02/screenshot-rustychoi_d2.png)

How do we specify the location of `d1` or `d1/f1.txt` using filepaths, if we are in `d2` and can't see them? We need some way 
of referring to the parent directory of `d2`.

Just like how `.` is a special way that means `current directory`, `..` means `parent directory of .`. So if we are inside `d2`
and we would like to specify the filepath to `f1.txt`, wei can use `../` to come up with the following path: `../d1/f1.txt`.
Your computer will then take the following steps to locate that file:
1. find `..`, which is the parent directory of `.`
1. find `d1` inside `..`
1. find `f1.txt` in `d1`

Now that we have `.` and `..`, we can refer to pretty much any file you want!
