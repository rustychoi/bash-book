# Mac & Linux

This subchapter is for Mac and Linux users.

## Installing Terminal

Linux and Mac come with the application `Terminal` pre-installed.

## Installing Bash

Linux and Mac almost always come with `Bash` pre-installed.
