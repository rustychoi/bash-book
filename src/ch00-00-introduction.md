# Introduction

Welcome to _Bash Fundamentals_, an introductory book about Bash.

## Who This Book is For

This book was designed to be standalone, and assumes no prior programming experience from the readers.

Whether you are completely new to programming or you are experienced, you should be able to follow the content of this book
in the order it is presented, and learn many fundamental aspects of Bash!

## Why Bash

### Productivity

TODO: Coming Soon!
