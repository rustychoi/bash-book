# Summary

[Bash Fundamentals](./title-page.md)
[Introduction](./ch00-00-introduction.md)

- [Installing Bash and Terminal](./ch01-00-installing-bash-and-terminal.md)
    - [Mac & Linux](./ch01-01-mac-and-linux.md)
    - [Windows](./ch01-02-windows.md)
    - [Configuring Bash and Terminal](./ch01-03-configuring-bash-and-terminal.md)
- [Filepaths](./ch02-00-filepaths-introduction.md)
    - [Relative Filepaths](./ch02-01-relative-filepaths.md)
    - [Demo using Terminal](./ch02-02-terminal-demo.md)
    - [Absolute Filepaths](./ch02-03-absolute-filepaths.md)
    - [Sample Problems](./ch02-04-sample-problems.md)
