# Filepaths

In this chapter we will cover the following:
1. the concept of _filepaths_, which is a way for computers to locate files and folders
1. various commands in Bash that
    1. help interact with the filesystem
    1. excercise the concept of filepaths

## Terminologies

1. the terms `folder` and `directory` are interchangeable; in computer science we use the term `directory` much more commonly
