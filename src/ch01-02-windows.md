# Windows

This subchapter is for Windows users.

## Installing Bash & Terminal

Let's click the following link [Git for Windows](https://gitforwindows.org), and you will see a page that looks like what's
shown below:

![gitforwindows](./img/ch01/screenshot-gitforwindows.png)

Let's click on the `Download` button, and scroll down all the way. You will see the following:

![gitbash-downloads](./img/ch01/screenshot-gitbash-downloads.png)

You most likely want to click on, and download the `Git-2.30.1-64-bit.exe`. Note that the version number may be different, but
that's okay. What matters is that it ends with `64-bit.exe`.

> There is a chance that your computer is old and is 32-bit. In that case, choose the one that ends with `32-bit.exe`.

After in downloads, click on what is downloaded and follow the instructions.
The default options and configurations (boxes that are checked in the installation helper) should be okay.

Whenever this book says to open `Terminal`, Windows users (you) should open `Git Bash`, which is an application that will
be downloaded during the installation process.
