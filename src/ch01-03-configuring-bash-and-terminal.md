# Configuring Bash and Terminal

Now that you have Terminal and Bash installed, let's configure `Bash` and `Terminal`.

Open the app `Terminal` (Windows users should open `Git Bash`).

> When you see blocks that look like what's directly below this paragraph, this most likely means that it is some command that
> we will type into Terminal. For example when you see the following, this means that we should literally type `mkdir d1` into
> the Terminal and press `Enter`. Feel free to copy and paste by dragging the text or using the copy button on the top right
> corner of the block.
> ```bash
> mkdir d1
> ```

Your terminal will probably look like the following, more or less:

![bash-profile-terminal-initial](./img/ch01/screenshot-terminal-initial.png)

## Check that `Bash` is running

Run the following command:
```bash
echo $0
```

If you type the above command and hit `Enter`, it will print something. If it prints something that ends with `bash`, you are 
all set and you can skip this section.

If yours does not end with `bash`, run the following command:
```bash
bash --login
```

Now if you run `echo $0` again, it should print something that ends with `bash`. From now on, make sure that every time you open
your Terminal, you run `bash --login`.

## Configure Bash

Visit the following link, [Bash Profile](https://gitlab.com/rustychoi/bash-book/-/blob/master/src/resource/.bash_profile), which
will take you to a page that looks like the following:

![bash-profile-main-page](./img/ch01/screenshot-bash-profile-main-page.png)

Press the following `Download` button, which will download a file called `src_resource_.bash_profile` to your `Downloads` 
folder.

![bash-profile-download-button](./img/ch01/screenshot-bash-profile-download-button.png)

Now let's copy and paste the following command to your Terminal and press `Enter`:
```bash
cat ~/Downloads/src_resource_.bash_profile >> ~/.bash_profile
```

Now lets run the following command:
```bash
source ~/.bash_profile
```

Your terminal should look something like this now:

![bash-profile-terminal-final](./img/ch01/screenshot-terminal-final.png)
