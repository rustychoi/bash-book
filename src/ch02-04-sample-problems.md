# Sample Problems

## Problem 1

Change your directory to the `Downloads` folder in your `HOME` directory.

<details>
<summary>Solution: click to expand</summary>
<p>

```bash
cd ~/Downloads/  # same as `cd ~/Downloads`
```

</p>
</details>

## Problem 2

Create a directory called `problem-2/` in the `Downloads` directory in your `HOME` directory, such that running the command
`find ~/Downloads/problem-2/` outputs the following:
```txt
/Users/<USERNAME>/Downloads/problem-2/
/Users/<USERNAME>/Downloads/problem-2/f3.txt
/Users/<USERNAME>/Downloads/problem-2/d1
/Users/<USERNAME>/Downloads/problem-2/d1/f2.txt
/Users/<USERNAME>/Downloads/problem-2/d1/d4
/Users/<USERNAME>/Downloads/problem-2/d1/d4/f1.txt
/Users/<USERNAME>/Downloads/problem-2/d1/d2
/Users/<USERNAME>/Downloads/problem-2/d1/d2/d3
```

Note the following:
1. `<USERNAME>` is just a placeholder and it will show your actual username
1. files whose names start with `f` are files
1. those whose names start with `d` are directories

<details>
<summary>Solution: click to expand</summary>
<p>

```bash
# first change directory
cd ~/Downloads/

# create directories
#
# the commands should roughly in this order
# because `mkdir` requires that parent directories exist
mkdir d1
mkdir d1/d2
mkdir d1/d2/d3
mkdir d1/d4

# create files
touch d1/d4/f1.txt
touch d1/f2.txt
touch f3.txt
```

</p>
</details>
