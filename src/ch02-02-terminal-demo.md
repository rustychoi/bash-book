# Demo using Terminal

Let's try this out in Terminal.

> Linux and Mac users should open the `Terminal` app, whereas Windows users should open `Git Bash`.

## Interacting with the Terminal

Terminal is an app where you type commands, and your computer carries out those commands and tells you the result.

Here are the typical steps that we take to interact with Terminal:
1. type a command
1. hit `Enter`, which indicates the end of the command
1. observe the result of running the command
1. wait for the Terminal to prompt you for another command
1. go back to step 1 and repeat

> Before we get started writing commands, let's first become familiar with how this book is structured.
>
> When you see blocks that look like what's directly below this paragraph, this most likely means that it is some command that
> we will type into Terminal. For example when you see the following, this means that we should literally type `mkdir d1` into
> the Terminal and press `Enter`. Feel free to copy and paste by dragging the text or using the copy button on the top right
> corner of the block.
> ```bash
> mkdir d1
> ```

> If there are multiple lines, like in the following block, this means that we should take the following actions, in order:
> 1. type `mkdir d1` in Terminal
> 1. hit `Enter`
> 1. type `touch f1.txt` in Terminal
> 1. hit `Enter`
> ```bash
> mkdir d1
> touch f1.txt
> ```

> Anything that follows `#` is a comment, including the `#` itself, and it's ignored by the Terminal. Comments are used to write
> descriptive notes into your code, so that you or anyone else can get a sense of what the code does without having to read the
> code itself.
>
> For example when you see the following block, you should only type `touch f1.txt` and `mkdir d1` into Terminal and ignore the
> rest.
> ```bash
> # this whole line is a comment and should be ignored
> touch f1.txt
> mkdir d1  # comments can be on the same as a command
> ```

> Lastly, we need a concise way to visualize the content of directories using text. Consider a directory called `rustychoi/`
> that contains `d1/` and `d2/` in it, and assume that `d1/` contains `f1.txt`. We will represent this using the following
> notation:
>
> ```txt
> rustychoi/
> ├── d1/
> │  └── f1.txt
> └── d2/
> ```
>
> This should be intuitive, but all it says is that `rustychoi/` contains `d1/` and `d2/`, and `d1/` contains `f1.txt`.

## Setup

> Let's make sure that typing in `echo $0` into your terminal prints something that ends with `bash`. If it does, you are all 
> set. if not, try running `bash --login`. Now if you run `echo $0` it should print something that ends with bash.
>
> If `echo $0` didn't print something that ended with `bash` without running `bash --login`, you will need to run 
> `bash --login` everytime you open up your terminal.

Copy and paste the following code into your terminal, and press `Enter`.

```bash
mkdir ~/Downloads/rustychoi && cd ~/Downloads/rustychoi/
```

This command simply sets up a dummy folder called `rustychoi` in your `Downloads` folder. You don't need to understand how
this works yet, just take it for granted and let's move on.

We will be modifying this folder `Downloads/rustychoi` throughout this subchapter, so feel free to open up that folder with
your file browser (`Finder` on Mac) to see what your commands are doing.

Let's start writing some commands!

## Command: `ls`

The first command that we'll learn is `ls`. `ls` is a command that is used to see what's in some directory. It has the form
`ls <PATH>` where `<PATH>` can be any arbitrary filepath to some directroy. To see what's inside the current directory, in
other words `.`, let's type this command into Terminal and hit `Enter`.
```bash
ls .
```

Nothing will be printed, because there is nothing in the directory `rustychoi` right now.

You can run `ls` without providing the path; `ls` will assume that you meant `ls .` and show you what is in the current
directory. So the following two commands are identical:
```bash
ls
ls .
```

Let's create the same folders and files as shown in the previous subchapter.

> At any point, feel free to run the following command in order to clear the content of your screen if you feel that your 
> screen is getting messy and full of previous commands and their results.
> ```bash
> clear
> ```

## Command: `mkdir`

`mkdir` is a command whose name comes from `make directory`, and it creates directories/folders. Just like `ls`, the `mkdir`
command takes the form `mkdir <PATH>` where `<PATH>` can be any arbitrary filepath. To create a directory called `d1` in the
current directory, let's use the following command:
```bash
mkdir d1
```

If you go to the directory `Downloads/rustychoi` using your operating system's file browser, you should see that there is now a
directory named `d1`! In other words,

Let's make `d2` as well. Note that directory names can optionally have a `/` at the end, therefore `d1` is equivalent to `d1/`.
Also recall that a relative path like `d1` is equivalent to `./d1`. Using these 2 rules, we can come to the conclusion that
all of the following commands are equivalent and specifies the same file:
```bash
mkdir d2
mkdir d2/    # optional `/` at the end of directory paths
mkdir ./d2   # optional `./` at the front
mkdir ./d2/  # optional `/` at the end, and `./` at the front
```

> From this point we will write directory paths with `/` at the end, in order to make it explicit whether or not paths specify a
> file or a directory. For example, assume that `f1` is a file and `d1/` is a directory.

You should now see both `d1/` and `d2/` inside `rustychoi/`.
You can check this using the `ls` command too; it should print something that shows both `d1/` and `d2/`.
Recall that all of the following commands are identical:
```bash
ls
ls .
ls ./
```

## Command `touch`

`touch` is like `mkdir`, except that it creates files instead of directories. The following command creates a file called
`f1.txt` inside `d1/`.
```bash
touch d1/f1.txt  # equivalent to `touch ./d1/f1.txt`
```

You can now go to `rustychoi/d1/` using your File browser and see that `f1.txt` has been created inside it. You can confirm
this by using the following as well:
```bash
ls d1/
```

We will talk more about files and how to work with files in the next chapter or two, so let's not worry too much about the
differences between files and directories.

## Command `cd`

Much like how you can double click a folder in your file browser and "go into" a directory, you can do the same by using the
`cd` command.

Let's make sure that we are in `rustychoi/` by running `ls ./` and seeing that it prints `d1/` and `d2/`.

If you run the following two commands, it will print `f1.txt`. This is because `cd d1/` takes you into the
`d1/` directory, and your current directory becomes `rustychoi/d1` instead of `rustychoi/`. So when `ls ./` is run, it only sees
the file `f1.txt`:
```bash
cd d1/
ls ./
```

You can go back up to `rustychoi/` by running:
```bash
cd ..  # equivalent to `cd ../`
```

## Command `find`

`ls` allows you to inspect the contents of a directory; more specifically, it allows you to see the content of the directory
one level deep. This is because running `ls ./` from inside `rustychoi/` will show `d1` and `d2` but will not show what's
inside `d1` or `d2`. What if you want to see every file and directory that some directory contains, such that running some
command shows `d1`, `d2` and `d1/f1.txt`? This is where the command `find` comes in.

Run the following command from inside `rustychoi/`, and you should see the following:
```bash
find .
```

`find` allows you to see __everything__ in a directory, at every depth.
```txt
.
./d1
./d1/f1.txt
./d2
```

Here is another example. If you want to see everything inside `d1/`, run the following:
```bash
find d1/  # practically the same as running `find d1`
```

## Command `rm`

> __IMPORTANT__: the command `rm` deletes files and directories and is extremely dangerous. This is becasue there is typically
> __NO__ way to get the file/dir back, once it's deleted. There is no _Trash Can_ equivalent for `rm`.
>
> Further more, `rm -r` is even more dangerous, because it deletes __EVERYTHING__ inside a directory. It's common for people to
> use `rm -r` without care and accidentally wipe a big portion of their computers clean. Use with caution!

If we can create directories and files, we should be able to delete them too. The command `rm`, which comes from the word
`remove`, will delete files and directories for us.

Here is an example of creating and deleting a __file__:
```bash
find ./       # see what's in the current directory
touch f3.txt  # create `f3.txt`
find ./       # confirm that `f3.txt` has been created

rm f3.txt  # remove `f3.txt`
find ./    # confirm that `f3.txt` is gone
```

Here is an example of deleting a __directory__. You can't delete directories using just `rm <FILEPATH>`; you have to use the
form `rm -r <FILEPATH>`:
```bash
find ./    # see what's in the current directory
mkdir d3/  # create `fd3/`
find ./    # confirm that `d3/` has been created

rm d3/  # try removing `d3/`
# Oops, this will fail and print a message like
# `rm: cannot remove 'd3': Is a directory`
find ./    # check that `d3/ has not been deleted

rm -r d3/  # try removing `d3/` again, this time using `-r`
find ./    # confirm that `d3` has been deleted
```
