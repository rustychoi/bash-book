# Absolute Paths

Relative paths are useful at times, but often times it's not enough.

Let's set up some files and directories to create a situation in which relative may not be that great. Run the following
commands:
```bash
mkdir d1/d3
mkdir d2/d3
touch d1/d3/f2.txt
touch d2/d3/f2.txt
```

One subtle thing I didn't mention before: when running `mkdir` or `touch`, the parent directory of the file/dir you are about
to create must exist. For example, the following commands will fail:
```bash
mkdir d4/d3      # fails because `d4` doesn't exist
touch d5/f3.txt  # fails because `d5` doesn't exist
```

The above commands create a directory structure that looks like the following:
```txt
.
├── d1/
│  ├── d3/
│  │  └── f2.txt
│  └── f1.txt
└── d2/
   └── d3/
      └── f2.txt
```
You can check this yourself using `find .` as well.

Let's pretend that your friend created this directory for you, and they want you to find `d3/f2.txt`. You have a problem: there
are 2 such files named `d3/f2.txt`.

Had your friend been a little more specific and said `d1/d3/f2.txt`, you would have no problem identifying this directory. But
what if there was another `d1/d3/f2.txt` somewhere else on your computer? Seems like relative paths are a bit ambiguous.

This is where _absolute filepaths_ come in. Try running the following command:
```bash
realpath d1/d3/f2.txt
```

The above command printed `/Users/jackwchoi/Downloads/rustychoi/d1/f1.txt` for me. Notice that this is a filepath, but is
different from any of the paths we have seen thus far. That's because it starts with a `/` at the front. Filepaths that start
with `/` are called _absolute filepaths_.

The `/` at the front of absolute paths has a special name: the `root` of your filesystem. The `root` of your filesystem is a
unique directory such that, for _every_ file on your computer if you were to trace the parent of that file and find the  parent
of that parent and repeat this process, you will end up at the root. The root has no parent directory and is literally the
`root` of all files and directories on your computer.

There is only one such root directory, and the fact that absolute paths start at root allows us and the computers to
unambiguously locate files.

Had your friend left you a note that said "I left you a file `/Users/jackwchoi/Downloads/rustychoi/d1/f1.txt`" you would have
been able to locate this file once and for all.

## Command `pwd`

`pwd` is another command that use absolute paths. It prints the absolute path to the directory that you are in.

Try using `cd` to navigate to different directories and running `pwd`. For example:
```bash
pwd  # should say that you are in `Downloads/`

cd d1/
pwd  # should say that you are in `Downloads/d1/`
```

As a fun fact, running `pwd` is equivalent to running `realpath .`

## The HOME directory: `~`

Just like `/`, there is another important directory you should be aware of: `~`.

The character `~` is called "tilde", and in Bash it means the `HOME` directory. Run the following:
```bash
realpath ~  # same as `realpath ~/`
```

For Mac users, `~` will be set to `/Users/<OS_USERNAME>` where `<OS_USERNAME>` is your operating system username. Different
operating systems will have slightly different dome directory paths.

`~` is important/useful for the following reasons:
1. when terminals open, unless configured otherwise, it will start at `~`
1. the vast majority of files and directories will be in `~`, including `Desktop/` and `Downloads/`
