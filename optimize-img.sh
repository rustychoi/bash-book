#!/usr/bin/env bash

declare -r ROOT=$(git rev-parse --show-toplevel)

find "$ROOT/src/img" -type f -name '*.png' |
    parallel 'oxipng --opt 3 --interlace 1 --strip safe {}'
